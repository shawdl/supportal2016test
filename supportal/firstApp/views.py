from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, render_to_response
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required 
from .models import Issue
from .forms import IssueForm, UserForm
from django.core.mail import send_mail
# Create your views here.

#Login page, uses index.html as template
def login(request):
	context = {
		"title": "Login"
	}
	return render(request, "index.html", context)

#New user page, creates a form to create new user, once new user is created redirects the user to the login page
#which the user can then immeadiately login
def newUser(request):
	if request.method == "POST":
		form = UserForm(request.POST)
		if form.is_valid():
			new_user = User.objects.create_user(**form.cleaned_data)
			return HttpResponseRedirect("/supportal/")
			#Code for emailing new user
			#send_mail(
			#	'Account' + new_user.username + 'created',
			#	'Your account with Supportal has been created. Thank you.',
			#	'supportal@oaklabs.io',
			#	[new_user.email],
			#)
	else:
		form = UserForm()

	return render(request, "adduser.html", {"form": form})

#Main page that is currently used as a list of a all issues in the database
@login_required
def mainPage(request):
	queryset = Issue.objects.all()
	context = {
		"Issue_list": queryset,
		"title": "Main"
	}
	return render(request, "index.html", context)

#View issues is empty, to be used to view issues
@login_required
def viewIssues(request):
	return HttpResponse("<h1>View Issues</h1>")

#Create issue is a form page to create an issue, currently does not actually create issues
@login_required
def createIssue(request):
	form = IssueForm()
	context = {
		"form": form,
	}
	return render(request, "issue_form.html", context)

#Delete issues is empty, to be used to close / delete issues
@login_required
def deleteIssues(request):
	return HttpResponse("<h1>Delete Issues</h1>")