from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from firstApp import views as firstapp_views

urlpatterns = [
	url(r'^main/$', firstapp_views.mainPage, name='main'),
	url(r'^$', auth_views.login, name='login'),
    url(r'^viewIssue/$', firstapp_views.viewIssues, name='viewIssues'),
    url(r'^create/$', firstapp_views.createIssue, name='createIssue'),
    url(r'^delete/$', firstapp_views.deleteIssues, name='viewIssues'),
    url(r'^newuser/$', firstapp_views.newUser, name='newUser'),
]
