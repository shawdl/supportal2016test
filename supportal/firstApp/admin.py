from django.contrib import admin

# Register your models here.
from .models import Issue

class issueAdmin(admin.ModelAdmin):
	list_display = ["__unicode__", "timestamp"]
	class Meta: 
		model = Issue

admin.site.register(Issue, issueAdmin)